#ifndef MYWINDOWOPTIONOPENCSV_H
#define MYWINDOWOPTIONOPENCSV_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QButtonGroup>
#include <QGroupBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QPushButton>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

class myWindowOptionOpenCSV : public QWidget
{
    Q_OBJECT
public:
    explicit myWindowOptionOpenCSV(QWidget *parent = nullptr);
    ~myWindowOptionOpenCSV();

public slots:
    void myWindowOptOpenCSV();
    void myStateCheckBox(int);

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *LayoutSeperatorOptions, *LayoutWerifyFile, *LayoutOtherOptions, *LayoutButtonCancOk;
    QButtonGroup *buttonGroup;
    QGroupBox *grBoxSeperatorOptions, *grBoxWerifyFile ,*grBoxOtherOptions;
    QCheckBox *chBoxTabulate, *chBoxComma, *chBoxSemicolon, *chBoxSpace, *chBoxOther,
              *chBoxFirstRowIsHead, *chBoxOnlyNumbers, *chBoxOnlyText, *chBoxTextAndNumbers;
    QLineEdit *lineEdOther;

    QRegularExpression regex;
    QRegularExpressionMatch match;

    QPushButton *buttonCancel, *buttonOk;

private slots:
    void setIdMyCheckBox(int);
    void getTextOnQlineEdit(const QString &);

signals:
    void idMyButtonCheckBox(int, QString, QString);
};

#endif // MYWINDOWOPTIONOPENCSV_H
