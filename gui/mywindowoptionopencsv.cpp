#include "gui/mywindowoptionopencsv.h"
#include <QDebug>

myWindowOptionOpenCSV::myWindowOptionOpenCSV(QWidget *parent) : QWidget(parent)
{
    mainLayout = new QVBoxLayout(); //![1]
//! [Group 1]
    grBoxSeperatorOptions = new QGroupBox(QString(tr("Opcje seperatora")), this);
    grBoxSeperatorOptions->setStyleSheet("QGroupBox{font-weight: bold; border: 1px solid gray; border-radius: 5px; margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin; subcontrol-position: top center; padding: 0 3px;}");
    grBoxSeperatorOptions->setEnabled(true);

    LayoutSeperatorOptions = new QHBoxLayout(grBoxSeperatorOptions);

    buttonGroup = new QButtonGroup(this);
    buttonGroup->setExclusive(false);

    chBoxTabulate = new QCheckBox(this);
    chBoxTabulate->setText(QString(tr("Tabulator")));

    chBoxComma = new QCheckBox(this);
    chBoxComma->setText(QString(tr("Przecinek")));

    chBoxSemicolon = new QCheckBox(this);
    chBoxSemicolon->setText(QString(tr("Średnik")));

    chBoxSpace = new QCheckBox(this);
    chBoxSpace->setText(QString(tr("Spacja")));

    chBoxOther = new QCheckBox(this);
    chBoxOther->setText(QString(tr("Inny")));

    lineEdOther = new QLineEdit(this);
    lineEdOther->setMaxLength(1);
    lineEdOther->setToolTip(QString(tr("Wpisz seperator jaki znajduje się w pliku")));
    lineEdOther->setFixedWidth(50);

    buttonGroup->addButton(chBoxTabulate,1);
    buttonGroup->addButton(chBoxComma,2);
    buttonGroup->addButton(chBoxSemicolon,3);
    buttonGroup->addButton(chBoxSpace,4);
    buttonGroup->addButton(chBoxOther,5);
//! [End Group 1]

//! [Group 2]
    grBoxWerifyFile = new QGroupBox(QString(tr("Parsowanie danych")), this);
    grBoxWerifyFile->setStyleSheet("QGroupBox{font-weight: bold; border: 1px solid gray; border-radius: 5px; margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin; subcontrol-position: top center; padding: 0 3px;}");
    grBoxWerifyFile->setEnabled(true);

    LayoutWerifyFile = new QHBoxLayout(grBoxWerifyFile);

    chBoxOnlyNumbers = new QCheckBox(this);
    chBoxOnlyNumbers->setText(QString(tr("Tylko liczby")));
    chBoxOnlyNumbers->setToolTip(QString(tr("Podczas wczytywania zakłada, że wczytywane są TYLKO LICZBY. Jeżeli w komórce będzie znak inny niż liczba zwraca błąd")));

    chBoxOnlyText = new QCheckBox(this);
    chBoxOnlyText->setText(QString(tr("Tylko tekst")));
    chBoxOnlyText->setToolTip(QString(tr("Podczas wczytywania zakłada, że wczytywane są TYLKO LITERY. Jeżeli w komórce będzie znak inny niż litera zwraca błąd")));

    chBoxTextAndNumbers = new QCheckBox(this);
    chBoxTextAndNumbers->setText(QString(tr("Tekst i liczby")));
    chBoxTextAndNumbers->setToolTip(QString(tr("Podczas wczytywania zakłada, że wczytywane są LICZBY I LITERY. Jeżeli w komórce będą znaki specjalne zwraca błąd")));
//! [End Group 2]

//! [Group 3]
    grBoxOtherOptions = new QGroupBox(QString(tr("Inne opcje")), this);
    grBoxOtherOptions->setStyleSheet("QGroupBox{font-weight: bold; border: 1px solid gray; border-radius: 5px; margin-top: 1ex;} QGroupBox::title{subcontrol-origin: margin; subcontrol-position: top center; padding: 0 3px;}");
    grBoxOtherOptions->setEnabled(true);

    LayoutOtherOptions = new QHBoxLayout(grBoxOtherOptions);

    chBoxFirstRowIsHead = new QCheckBox(this);
    chBoxFirstRowIsHead->setText(QString(tr("Pierwszy wiersz jest nagłówkiem")));
//! [End Group 3]

    LayoutButtonCancOk = new QHBoxLayout(); //![2]

    buttonCancel = new QPushButton(this);
    buttonCancel->setText(QString(tr("Anuluj")));

    buttonOk = new QPushButton(this);
    buttonOk->setText(QString(tr("Ok")));
//! [Layout Group 1]
    LayoutSeperatorOptions->addWidget(chBoxTabulate);
    LayoutSeperatorOptions->addWidget(chBoxComma);
    LayoutSeperatorOptions->addWidget(chBoxSemicolon);
    LayoutSeperatorOptions->addWidget(chBoxSpace);
    LayoutSeperatorOptions->addWidget(chBoxOther);
    LayoutSeperatorOptions->addWidget(lineEdOther);
//! [End Layout Group 1]

//! [Layout Group 2]
    LayoutWerifyFile->addWidget(chBoxOnlyNumbers);
    LayoutWerifyFile->addWidget(chBoxOnlyText);
    LayoutWerifyFile->addWidget(chBoxTextAndNumbers);
//! [End Layout Group 2]

    LayoutOtherOptions->addWidget(chBoxFirstRowIsHead);

    LayoutButtonCancOk->addWidget(buttonCancel);
    LayoutButtonCancOk->addWidget(buttonOk);

    setLayout(mainLayout);
    mainLayout->addWidget(grBoxSeperatorOptions);
    mainLayout->addWidget(grBoxWerifyFile);
    mainLayout->addWidget(grBoxOtherOptions);
    mainLayout->addLayout(LayoutButtonCancOk);

    QObject::connect(chBoxOther, &QCheckBox::toggled, lineEdOther, &QLineEdit::setEnabled);
    QObject::connect(chBoxOther, &QCheckBox::toggled, chBoxTabulate, &QCheckBox::setDisabled);
    QObject::connect(chBoxOther, &QCheckBox::toggled, chBoxComma, &QCheckBox::setDisabled);
    QObject::connect(chBoxOther, &QCheckBox::toggled, chBoxSemicolon, &QCheckBox::setDisabled);
    QObject::connect(chBoxOther, &QCheckBox::toggled, chBoxSpace, &QCheckBox::setDisabled);

//    QObject::connect(chBoxOther, &QCheckBox::stateChanged, this, &myWindowOptionOpenCSV::myStateCheckBox/*, Qt::QueuedConnection*/);
    QObject::connect(chBoxOther, &QCheckBox::stateChanged, this, &myWindowOptionOpenCSV::myStateCheckBox);
    QObject::connect(lineEdOther, &QLineEdit::textEdited, this, &myWindowOptionOpenCSV::getTextOnQlineEdit);
    QObject::connect(buttonGroup, &QButtonGroup::idClicked, this, &myWindowOptionOpenCSV::setIdMyCheckBox);

    QObject::connect(chBoxOnlyNumbers, &QCheckBox::toggled, chBoxOnlyText, &QCheckBox::setDisabled);
    QObject::connect(chBoxOnlyText, &QCheckBox::toggled, chBoxOnlyNumbers, &QCheckBox::setDisabled);

    QObject::connect(chBoxTextAndNumbers, &QCheckBox::toggled, chBoxOnlyNumbers, &QCheckBox::setDisabled);
    QObject::connect(chBoxTextAndNumbers, &QCheckBox::toggled, chBoxOnlyText, &QCheckBox::setDisabled);

    QObject::connect(buttonCancel, &QPushButton::clicked, this, &myWindowOptionOpenCSV::close);
}

void myWindowOptionOpenCSV::myWindowOptOpenCSV()
{
    this->setFixedHeight(300); //wysokość
    this->setFixedWidth(500); //szerokość
    this->show();
}

void myWindowOptionOpenCSV::myStateCheckBox(int s)
{
    qDebug()<< s;
}

void myWindowOptionOpenCSV::setIdMyCheckBox(int idCheckBox)
{
    if(idCheckBox==1){ //Tabulator
        qDebug()<<"id CheckBoxa" << idCheckBox << chBoxTabulate->text() << "\t";
        emit idMyButtonCheckBox(idCheckBox, chBoxTabulate->text(), "\t");
    }
    if(idCheckBox==2){ //Przecinek
        qDebug()<<"id CheckBoxa" << idCheckBox << chBoxComma->text() << ",";
        emit idMyButtonCheckBox(idCheckBox, chBoxComma->text(), ",");
    }
    if(idCheckBox==3){ //Średnik
        qDebug()<<"id CheckBoxa" << idCheckBox << chBoxSemicolon->text() << ";";
        emit idMyButtonCheckBox(idCheckBox, chBoxSemicolon->text(), ";");
    }
    if(idCheckBox==4){ //Spacja
        qDebug()<<"id CheckBoxa" << idCheckBox << chBoxSpace->text();
        emit idMyButtonCheckBox(idCheckBox, chBoxSpace->text(), " ");
    }
    if(idCheckBox==5){ //Inny
        if(lineEdOther->text().isEmpty()){
            return;
        }
        else {
            qDebug()<<"id CheckBoxa" << idCheckBox << chBoxOther->text() << lineEdOther->text();
        emit idMyButtonCheckBox(idCheckBox, chBoxOther->text(), lineEdOther->text());
        }
    }
}

void myWindowOptionOpenCSV::getTextOnQlineEdit(const QString &txt)
{
    qDebug()<< txt;
    regex.setPattern(QString("[^A-Za-z0-9\\p{L}]"));
    match = regex.match(txt);
    if(match.hasMatch()){
        qDebug()<< "Dobrze";
    }
    else{
        qDebug()<< "Podaj wymagane znaki";
    }
}

myWindowOptionOpenCSV::~myWindowOptionOpenCSV()
{
    delete LayoutButtonCancOk; //![2]
    delete mainLayout; //![1]
}
