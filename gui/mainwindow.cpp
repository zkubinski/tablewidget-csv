#include "gui/mainwindow.h"
#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    resize(600, 370);
    mWidget = new QWidget(); //![1]
    mLayout = new QVBoxLayout(mWidget); //![2]

    pbLayoutTable = new QHBoxLayout(); //![3]
    pbLayoutMain = new QHBoxLayout(); //![4]

    pbOpenFile = new QPushButton(mWidget);
    pbOpenFile->setText("Open CSV file");

    pbSaveFile = new QPushButton(mWidget);
    pbSaveFile->setText("Save as CSV file");

    pbQuit = new QPushButton(mWidget);
    pbQuit->setText("Quit");

    pbAddRow = new QPushButton(mWidget);
    pbAddRow->setText("Add row");

    pbRemoveRow = new QPushButton(mWidget);
    pbRemoveRow->setText("Remove row");

    tableWidg = new QTableWidget(mWidget);
//    tableWidg->insertRow(0);
//    tableWidg->insertColumn(0);
//    QTableWidgetItem *item = new QTableWidgetItem();
//    item->setText("sad");
//    tableWidg->setItem(0,0,item);

    myWindowCSV = new myWindowOptionOpenCSV();

    pbLayoutTable->addWidget(pbAddRow);
    pbLayoutTable->addWidget(pbRemoveRow);

    pbLayoutMain->addWidget(pbOpenFile);
    pbLayoutMain->addWidget(pbSaveFile);
    pbLayoutMain->addWidget(pbQuit);

    mLayout->addWidget(tableWidg);
    mLayout->addLayout(pbLayoutTable);
    mLayout->addLayout(pbLayoutMain);

    //add row button
    QObject::connect(pbAddRow, &QPushButton::clicked, this, &MainWindow::myAddRow);
    QObject::connect(this, &MainWindow::myRow, tableWidg, &QTableWidget::insertRow);

    //remove row button
    QObject::connect(pbRemoveRow, &QPushButton::clicked, this, &MainWindow::myRemRow);

    //open file button
    QObject::connect(pbOpenFile, &QPushButton::clicked, this, &MainWindow::openFileDialog);

    //quit button
    QObject::connect(pbQuit, &QPushButton::clicked, this, &MainWindow::close);

    setCentralWidget(mWidget);
}

void MainWindow::myAddRow()
{
    int myRowCnt;
    myRowCnt = tableWidg->rowCount();

    if(myRowCnt == 0){
        emit myRow(myRowCnt);

        for(int i=0; i<5; ++i){
            tableWidg->insertColumn(i);
        }
    }
    else{
        emit myRow(myRowCnt);
    }
}

void MainWindow::myRemRow()
{
    tableWidg->removeRow(tableWidg->currentRow());

    if(tableWidg->rowCount() == 0){
        for(int i=5; i>=0; --i){
            tableWidg->removeColumn(i);
        }
    }
}

void MainWindow::openFileDialog()
{
//    QRegularExpression regex("[a-z0-9_@\\-^!#$%&+={} .+)]+\\.[a-z]+$");
    QRegularExpression regex("[^\\/:*?\"<>|\r\n]+$");
    QRegularExpressionMatch match;

    QString fileName;
    QString filter = "All Files (*.*) ;; Text File (*.txt) ;; CSV File (*.csv)";
    QString defaultFilter = "CSV File (*.csv)";

    fileName = QFileDialog::getOpenFileName(this, QString("Open file"), QDir::homePath(), filter, &defaultFilter);

    if(fileName.isEmpty()){
        return;
    }
    else{
        OpenCSV.myOpenFile(fileName);

        if(OpenCSV.myOpenFile()){
            for(int i=0; i<OpenCSV.myRow(); ++i){
                tableWidg->insertRow(i);
            }

            for(int j=0; j<OpenCSV.myColumns(); ++j){
                tableWidg->insertColumn(j);
            }
        }
        match = regex.match(fileName);
        if(match.hasMatch()){
            qDebug()<< match.captured(0);
        }
        myWindowCSV->myWindowOptOpenCSV();
    }
}

MainWindow::~MainWindow()
{
    delete pbRemoveRow;
    delete pbAddRow;
    delete pbQuit;
    delete pbSaveFile;
    delete pbOpenFile;
    delete pbLayoutMain; //![4]
    delete pbLayoutTable; //![3]
    delete mLayout; //![2]
    delete mWidget; //![1]
}
