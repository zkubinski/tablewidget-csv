#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QTableWidget>
#include <QFileDialog>
#include "engine/myopencsv.h"
#include "gui/mywindowoptionopencsv.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QWidget *mWidget;
    QVBoxLayout *mLayout;
    QHBoxLayout *pbLayoutTable;
    QHBoxLayout *pbLayoutMain;

    QTableWidget *tableWidg;

    myWindowOptionOpenCSV *myWindowCSV;
    myOpenCSV OpenCSV;
    QPushButton *pbOpenFile, *pbSaveFile, *pbQuit, *pbAddRow, *pbRemoveRow;

private slots:
    void myAddRow();
    void myRemRow();
    void openFileDialog();

signals:
    void myRow(int);
};

#endif // MAINWINDOW_H
