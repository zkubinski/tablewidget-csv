#ifndef MYWINDOWOPTIONSAVECSV_H
#define MYWINDOWOPTIONSAVECSV_H

#include <QWidget>

class myWindowOptionSaveCSV : public QWidget
{
    Q_OBJECT
public:
    explicit myWindowOptionSaveCSV(QWidget *parent = nullptr);
    ~myWindowOptionSaveCSV();

public slots:
    void myWindowOptSaveCSV();

signals:

};

#endif // MYWINDOWOPTIONSAVECSV_H
