#include "myopencsv.h"

#include <QString>
#include <QStringList>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QVector>

#include <QDebug>

myOpenCSV::myOpenCSV()
{}

void myOpenCSV::myOpenFile(QString &fileName)
{
    FileName = fileName;
    QString strData;
    QStringList field;
    QVector<unsigned int> myVectColumns;
    unsigned int sumColumns=0;
    int _columns=0, _rows=0;

    QFile openFile(FileName);

    if(!openFile.open(QFile::ReadOnly | QFile::Text)){
        QMessageBox::information(this, "Otwieranie pliku...", "Nie otwarto pliku", QMessageBox::Close);
    }
    else
    {
        QTextStream streamFile (&openFile);

        while(!streamFile.atEnd()){ //wczytuję plik do momentu aż osiągnie on EOF (koniec pliku)
            strData = streamFile.readLine(); //wczytuję plik linia po linii z utratą \n

            for(int i=0; i<strData.size(); ++i){ //sprawdzam czy jest seperator kolumn, jeżeli jest, to zliczam ile ich jest
                if(strData.at(i)==";" || strData.at(i)==":" || strData.at(i)=="," || strData.at(i)==" "){
                    _columns++;
                }
            }

            field << strData; //wczytuję dane linia po linii do kontenera

            _rows++; //zliczam ile jest wierszy w pliku

            myVectColumns.push_back(_columns); //zapisuję (wstępną) zliczaną po seperatorach ilość kolumn każdego wiersza, posłuży to do sprawdzenia czy plik ma wszystkie kolumny równe
            //qDebug()<<"wiersz"<<rows<<"ma kolumn"<<columns;
            _columns=0; //zeruję ilość kolumn aby nie sumowało wszystkich kolumn z każdego wiersza na raz, gdyż ilość kolumn jest indywidualna dla każdego wiersza
            //czyli nowy wiersz ma ZERO kolumn i zliczanie odbywa się na nowo dla każdego wiersza
        }

        for(int i=0; i<myVectColumns.size(); ++i){ //pętla sumująca wszystkie kolumny
            sumColumns = sumColumns + myVectColumns.at(i)+1;
        }

        if(sumColumns % _rows == 0){ //warunek sprawdzający czy plik ma wszystkie kolumny po równo
            bOpenFile=true;

            columns = (sumColumns / _rows);
            rows = _rows;

            QMessageBox::information(this, "Ładowanie danych...", "Dane zostały załadowane", QMessageBox::Close);
        }
        if(sumColumns % _rows == 1){
            bOpenFile=false;

            columns=0;
            rows=0;

            QMessageBox::warning(this, "Ładowanie danych...", "Dane nie zostały załadowane\nKolumny nie mają tego samego rozmiaru", QMessageBox::Ok);
            return;
        }

//        openFile.flush();
        openFile.close();
    }

    qDebug()<< field;
    qDebug()<< FileName;
}

int myOpenCSV::myRow()
{
    return rows;
}

int myOpenCSV::myColumns()
{
    return columns;
}

bool myOpenCSV::myOpenFile()
{
    return bOpenFile;
}

myOpenCSV::~myOpenCSV()
{
}
