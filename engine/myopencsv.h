#ifndef MYOPENCSV_H
#define MYOPENCSV_H

#include <QWidget>

class myOpenCSV : public QWidget
{
public:
    myOpenCSV();
    ~myOpenCSV();

    int myRow();
    int myColumns();

    void myOpenFile(QString &);
    bool myOpenFile();

private:
    bool bOpenFile=false;
    int rows=0;
    int columns=0;
    QWidget *_parent;
    QString FileName;
};

#endif // MYOPENCSV_H
